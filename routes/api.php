<?php
use Illuminate\Http\Request;
Route::group(['namespace'=>'Api','middleware' => 'cors'], function () {
    Route::post("/auth","UserController@auth");  
   /* RUTAS PUBLICAS */
   Route::resource('region','RegionController');
   Route::get('city/find_by_region_id/{id}','CityController@find_by_region_id');
   Route::resource('city','CityController');
   Route::get('province/find_by_region_id/{id}','ProvinceController@find_by_region_id');
   Route::resource('province','ProvinceController'); 
   Route::get('commune/find_by_province_id/{id}','CommuneController@find_by_province_id');
   Route::resource('commune','CommuneController'); 
   Route::resource('profesion','ProfesionController');
   Route::resource('academic_level','AcademicLevelController');
   Route::resource('publish_document','PublishDocumentController');
   Route::resource('document','DocumentController');
   
   
   Route::get('stablishment','StablishmentController@index');
   Route::post('preference', 'PreferenceController@store');
   /* FIN RUTAS PUBLICAS */


    /* RUTAS ADMINISTRDOR */
    Route::group(['prefix' => 'administrator'], function ()
       {
        Route::group(['middleware'=>'jwt.auth','middleware' => 'permission-role:1'],function(){
        Route::get('user/list','UserController@listar');
           Route::put('user/change_passwd/{id}','UserController@change_passwd');
           Route::put('user/profile/{id}','UserController@profile');
           Route::resource('user','UserController');   
           Route::get('role/listar','RoleController@listar');   
           Route::resource('region','RegionController');  
           Route::get('province/find_by_region_id/{id}','ProvinceController@find_by_region_id');
           Route::resource('province','ProvinceController'); 
           Route::get('commune/find_by_province_id/{id}','CommuneController@find_by_province_id');
           Route::resource('commune','CommuneController');  
           Route::get('city/find_by_region_id/{id}','CityController@find_by_region_id');
           Route::resource('city','CityController'); 
           Route::resource('profesion','ProfesionController');
           Route::resource('publish_document','PublishDocumentController');
           Route::resource('document','DocumentController');
        });
    });

    /* RUTAS ADMINISTRATIVO */
    Route::group(['prefix' => 'administrative'], function ()
       {
        Route::group(['middleware'=>'jwt.auth','middleware' => 'permission-role:2'],function(){
           Route::put('user/change_passwd/{id}','UserController@change_passwd');
           Route::put('user/profile/{id}','UserController@profile');
           Route::resource('user','UserController');
           Route::get('role/listar','RoleController@listar');   
           Route::resource('region','RegionController');
           Route::get('province/find_by_region_id/{id}','ProvinceController@find_by_region_id');
           Route::resource('province','ProvinceController'); 
           Route::get('commune/find_by_province_id/{id}','CommuneController@find_by_province_id');
           Route::resource('commune','CommuneController'); 
           Route::get('city/find_by_region_id/{id}','CityController@find_by_region_id');
           Route::resource('city','CityController');
           Route::resource('profesion','ProfesionController');
           Route::resource('academic_level','AcademicLevelController');
           Route::get('publish_document/count_tot_profesion_id/{profesion_id}','PublishDocumentController@count_tot_profesion_id');
           Route::get('publish_document/export','PublishDocumentController@export');
           Route::get('publish_document/getTot','PublishDocumentController@getTot');
           Route::get('publish_document/getTotComuna/{id1}/{id2}','PublishDocumentController@getTotComuna');
           
           Route::resource('publish_document','PublishDocumentController');
           
           Route::get('document/download_zip/{id}','DocumentController@download_zip');
           Route::get('document/remove_zip/{id}','DocumentController@remove_zip');
           Route::resource('document','DocumentController');
          
          
           Route::resource('stablishment','StablishmentController');
           Route::resource('preference','PreferenceController');
        });
    });
});
