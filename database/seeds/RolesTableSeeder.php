<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
           'type' => 'administrator',
           'created_at' => Carbon\Carbon::now(),
           'updated_at' => Carbon\Carbon::now(),
           'deleted_at' => null
        ]);
        
        DB::table('roles')->insert([
           'type' => 'administrative',
           'created_at' => Carbon\Carbon::now(),
           'updated_at' => Carbon\Carbon::now(),
           'deleted_at' => null
        ]);
    }
}
