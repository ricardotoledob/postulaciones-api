<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
class RegionSeeder extends Seeder
{
    public function run()
    {
        $file = base_path('public/csv/regions.csv');
        DB::table('regions')->delete();
        $csv= new CsvFile($file);
        foreach($csv AS $row) {
             App\Region::create([
                 'id' => $row[0], 
                 'name' => $row[1],
                 'created_at' => Carbon\Carbon::now(),
                 'updated_at' => Carbon\Carbon::now(),
                 'deleted_at' => null
             ]);
        }
    }
}
