<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Ricardo',
            'apepat'=>'Toledo',
            'apemat'=> 'Barria',
            'rut'=>'155825170',
            'email'=>'ricardo.toledo.b@redsalud.gov.cl',
            'password'=>bcrypt('secret'),
            'firm'=>'',
            'created_at'=>Carbon\Carbon::now(),
            'updated_at'=>Carbon\Carbon::now(),
            'deleted_at'=>null,
            'role_id'=>1,
        ]);

        DB::table('users')->insert([
            'name'=>'Administrativo',
            'apepat'=>'Administrativo',
            'apemat'=> 'Administrativo',
            'rut'=>'111111111',
            'email'=>'administrativo@redsalud.gov.cl',
            'password'=>bcrypt('secret'),
            'firm'=>'',
            'created_at'=>Carbon\Carbon::now(),
            'updated_at'=>Carbon\Carbon::now(),
            'deleted_at'=>null,
            'role_id'=>2,
        ]);
    }
}
