<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
class ProvinceSeeder extends Seeder
{
    public function run()
    {
        $file = base_path('public/csv/provinces.csv');
        DB::table('provinces')->delete();
        $csv= new CsvFile($file);
        foreach($csv AS $row) {
             App\Province::create([
                 'id' => $row[0], 
                 'name' => $row[1],
                 'region_id'=>$row[2],
                 'created_at' => Carbon\Carbon::now(),
                 'updated_at' => Carbon\Carbon::now(),
                 'deleted_at' => null
             ]);
        }
    }
}
