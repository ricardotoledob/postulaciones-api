<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
class CommuneSeeder extends Seeder
{
    public function run()
    {
        $file = base_path('public/csv/communes.csv');
        DB::table('communes')->delete();
        $csv= new CsvFile($file);
        foreach($csv AS $row) {
             App\Commune::create([
                 'id' => $row[0], 
                 'name' => $row[1],
                // 'deis_code'=>$row[2],
                 'province_id'=>$row[3],
                 'created_at' => Carbon\Carbon::now(),
                 'updated_at' => Carbon\Carbon::now(),
                 'deleted_at' => null
             ]);
        }
    }
}
