<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublishDocumentsTable extends Migration
{
    public function up() {
        Schema::create('publish_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('apepat');
            $table->string('apemat');
            $table->string('rut');
            $table->string('email');
            $table->string('phone1');
            $table->string('phone2')->nullable();
            $table->string('state1')->nullable();
            $table->string('state2')->nullable();
            $table->string('description');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('academic_level_id')->unsigned();
            $table->foreign('academic_level_id')->references('id')->on('academics_levels');
            $table->integer('profesion_id')->unsigned();
            $table->foreign('profesion_id')->references('id')->on('profesions');
            $table->integer('commune_id')->unsigned();
            $table->foreign('commune_id')->references('id')->on('communes');
  
        });
    }    

    public function down() {
       Schema::drop('publish_documents');
    }
}
