<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicLevelsTable extends Migration
{
    public function up() {
        Schema::create('academics_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
         
        });
    }    

    public function down() {
       Schema::drop('academics_levels');
    }
}
