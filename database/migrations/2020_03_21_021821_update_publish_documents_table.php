<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePublishDocumentsTable extends Migration
{
    public function up() {
        Schema::table('publish_documents', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
        });
    }    

    public function down() {
       Schema::drop('publish_documents');
    }
}
