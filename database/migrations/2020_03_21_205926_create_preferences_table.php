<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferencesTable extends Migration
{
    public function up() {
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('id');
           
            $table->timestamps();
            $table->softDeletes();
            $table->integer('publish_document_id')->unsigned();
            $table->foreign('publish_document_id')->references('id')->on('publish_documents');
            $table->integer('stablishment_id')->unsigned();
            $table->foreign('stablishment_id')->references('id')->on('stablishments');
       
        });
    }    

    public function down() {
       Schema::drop('preferences');
    }
}
