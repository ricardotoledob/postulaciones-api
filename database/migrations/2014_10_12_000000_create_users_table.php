<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('apepat');
            $table->string('apemat');
            $table->string('rut')->nullable()->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('firm');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }    

    public function down() {
       Schema::drop('users');
    }
}
