<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    public function up() {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('file');
            $table->string('title');
            $table->text('description');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('publish_document_id')->unsigned();
            $table->foreign('publish_document_id')->references('id')->on('publish_documents');
         
        });
    }    

    public function down() {
       Schema::drop('documents');
    }
}
