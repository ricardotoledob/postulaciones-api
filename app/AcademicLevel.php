<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AcademicLevel extends Model
{
    use SoftDeletes;
    protected $table='academics_levels';
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];
}
