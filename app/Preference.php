<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Preference extends Model
{
    use SoftDeletes;
    protected $table='preferences';
       protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];

     public function publish_document(){
         return $this->belongsTo('App\PublishDocument');
     }
     public function stablishment(){
         return $this->belongsTo('App\Stablishment');
     }
}
