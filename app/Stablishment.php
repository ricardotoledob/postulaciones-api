<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Stablishment extends Model
{
    use SoftDeletes;
    protected $table='stablishments';
    protected $fillable=[
        'name'
    ];
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];
   
}
