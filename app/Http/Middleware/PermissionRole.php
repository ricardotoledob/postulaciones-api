<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
class PermissionRole
{
    public function handle($request, Closure $next,$role)
    {
        $user = \JWTAuth::parseToken()->authenticate();
        if ($user->role_id == $role) {
           return $next($request);
        }
         return response()->json(['message' => 'No tiene permisos necesarios'], 501);
    }
}
