<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordResetRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'current_password'=> 'required|no_password',
            'password' => 'required|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'current_password.no_password' => 'Clave actual incorrecta!',
            'current_password.required' => ' La clave actual es requerida!',
            'password.confirmed' => 'No coinciden las claves',
            'password.required' => 'Debe ingresar nueva clave'
        ];
    }
}
