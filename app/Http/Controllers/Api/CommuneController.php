<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Commune;
use Yajra\Datatables\Facades\Datatables;
class CommuneController extends Controller
{
    public function destroy($id){
        $commune=Commune::findOrFail($id);
        $commune->delete();
        return response()->json(compact('commune'));
    }

    public function index(){
        $commune=Commune::all();
        return Datatables::of($commune)
        ->addColumn('action', function($commune) {
          return '<a href="#" class="edit" data-id='.$commune->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$commune->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }

    public function show($id){
        $commune=Commune::with(['province.region'])->findOrfail($id);
        return response()->json(compact('commune'));
    }

    public function find_by_province_id($id){
        $commune=Commune::where('province_id','=',$id)->get();
        return response()->json(compact('commune'));
    }
    
     public function store(Request $request){
        $commune=new Commune();
        $commune->name=$request->name;
        $commune->province_id=$request->province_id;
        $commune->save();
        return response()->json(compact('commune'));
    }

       
    
        public function edit($id){
           $commune=Commune::findOrFail($id);
           return response()->json(compact('commune'));
        }
       
   
       
        public function update($id,Request $request){
            $commune=Commune::findOrFail($id);
            $commune->name=$request->name;
            $commune->province_id=$request->province_id;
            $commune->save();
            return response()->json(compact('commune'));
        }
}
