<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Region;
use Yajra\Datatables\Facades\Datatables;
class RegionController extends Controller
{
    public function destroy($id){
        $regions=Region::findOrFail($id);
        $regions->delete();
        return response()->json(compact('regions'));
    }

    public function index(){
        $regions=Region::all();
        return Datatables::of($regions)
        ->addColumn('action', function($regions) {
          return '<a href="#" class="edit" data-id='.$regions->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$regions->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }

    public function show($id){
        $regions=Region::findOrfail($id);
        return response()->json(compact('regions'));
    }
    
     public function store(Request $request){
        $regions=new Region();
        $regions->name=$request->name;
        $regions->save();
        return response()->json(compact('regions'));
    }

       
    
        public function edit($id){
           $regions=Region::findOrFail($id);
           return response()->json(compact('regions'));
        }
       
   
       
        public function update($id,Request $request){
            $regions=Region::findOrFail($id);
            $regions->name=$request->name;
            $regions->save();
            return response()->json(compact('regions'));
        }
}
