<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use App\PublishDocument;

class PublishDocumentController extends Controller
{
    public function destroy($id){
        $publish_document=PublishDocument::findOrFail($id);
        $publish_document->delete();
        return response()->json(compact('publish_document'));
    }

    public function index(){
        $publish_document=PublishDocument::with(['documents','commune.province.region','academic_level','profesion','preferences.stablishment'])->get();
        return Datatables::of($publish_document)
        ->addColumn('action', function($publish_document) {
          return '<a href="#" class="edit" data-id='.$publish_document->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$publish_document->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }

    public function export(){
        Excel::create('Reporte Detallado', function($excel) {
            $excel->sheet('Detalle de Publicaciones', function($sheet) {
                $publish_documents=PublishDocument::all();
                foreach ($publish_documents as $key => $value) {


                    $payload[] = array('Id' => $value['id'],
                                       'Nombre' => $value['name'],
                                       '1er apellido' => $value['apepat'],
                                       '2do apellido' => $value['apemat'],
                                       'Rut' => $value['rut'],
                                       'Correo Electronico' => $value['email'],
                                       'Telefono 1' => $value['phone1'],
                                       'Telefono 2' => $value['phone2'],
                                       'Descripcion' => $value['description'],
                                       'Profesion' => $value['profesion']['name'],
                                       'Nivel Acadèmico' => $value['academic_level']['name'],
                                       'Comuna' => $value['commune']['name'],
                                       'Provincia' => $value['commune']['province']['name'],
                                       'Region' => $value['commune']['province']['region']['name'],
                                    );


                }
                $sheet->fromArray($payload);
            });
        })->download('xls',['Access-Control-Allow-Origin'=>'*']);
    }


    public function getTot(){
        $publish_document=PublishDocument::distinct('rut')->count('rut');
        return $publish_document;
    }

    public function getTotComuna($id_profesion,$id_comuna){
        $publish_document=PublishDocument::
                        where('commune_id','=',$id_comuna)
                        ->where('profesion_id','=',$id_profesion)
                        ->distinct('rut')->count('rut');
        return $publish_document;
    }

    public function count_tot_profesion_id($profesion_id){
        $publish_document=PublishDocument::where('profesion_id','=',$profesion_id)->distinct('rut')->count('rut');
        return $publish_document;
    }


    public function show($id){
        $publish_document=PublishDocument::with(['commune.province.region','academic_level','profesion','documents','preferences.stablishment'])->findOrfail($id);
        return response()->json(compact('publish_document'));
    }
    
     public function store(Request $request){
        $publish_document=new PublishDocument();
        $publish_document->name=$request->name;
        $publish_document->apepat=$request->apepat;
        $publish_document->apemat=$request->apemat;
        $publish_document->rut=$request->rut;
        $publish_document->email=$request->email;
        $publish_document->phone1=$request->phone1;
        $publish_document->phone2=$request->phone2;
        $publish_document->state1=$request->state1;
        $publish_document->state2=$request->state2;
        $publish_document->description=$request->description;
        $publish_document->profesion_id=$request->profesion_id;
        $publish_document->academic_level_id=$request->academic_level_id;
        $publish_document->commune_id=$request->commune_id;  
        $publish_document->save();
        return response()->json(compact('publish_document'));
    }

       
    
        public function edit($id){
           $publish_document=PublishDocument::findOrFail($id);
           return response()->json(compact('publish_document'));
        }
       
   
       
        public function update($id,Request $request){
            $publish_document=PublishDocument::findOrFail($id);
            $publish_document->name=$request->name;
            $publish_document->apepat=$request->apepat;
            $publish_document->apemat=$request->apemat;
            $publish_document->rut=$request->rut;
            $publish_document->email=$request->email;
            $publish_document->phone1=$request->phone1;
            $publish_document->phone2=$request->phone2;
            $publish_document->state1=$request->state1;
            $publish_document->state2=$request->state2;
            $publish_document->description=$request->description;
            $publish_document->profesion_id=$request->profesion_id;
            $publish_document->academic_level_id=$request->academic_level_id;
            $publish_document->commune_id=$request->commune_id;  
            $publish_document->save();
            return response()->json(compact('publish_document'));
        } 
}
