<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use Yajra\Datatables\Facades\Datatables;
class RoleController extends Controller
{
    public function listar(){
        $roles=Role::all();
        return response()->json(compact('roles'));
    }
}
