<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Province;
use Yajra\Datatables\Facades\Datatables;

class ProvinceController extends Controller
{
    public function destroy($id){
        $province=Province::findOrFail($id);
        $province->delete();
        return response()->json(compact('province'));
    }

    public function index(){
        $province=Province::all();
        return Datatables::of($province)
        ->addColumn('action', function($province) {
          return '<a href="#" class="edit" data-id='.$province->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$province->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }



    public function show($id){
        $province=Province::with(['region'])->findOrfail($id);
        return response()->json(compact('province'));
    }
    
     public function store(Request $request){
        $province=new Province();
        $province->name=$request->name;
        $province->region_id=$request->region_id;
        $province->save();
        return response()->json(compact('province'));
    }

    public function find_by_region_id($id){
        $province=Province::where('region_id','=',$id)->get();
        return response()->json(compact('province'));
    }
       
    
        public function edit($id){
           $province=Province::findOrFail($id);
           return response()->json(compact('province'));
        }
       
   
       
        public function update($id,Request $request){
            $province=Province::findOrFail($id);
            $province->name=$request->name;
            $province->region_id=$request->region_id;
            $province->save();
            return response()->json(compact('province'));
        }
}
