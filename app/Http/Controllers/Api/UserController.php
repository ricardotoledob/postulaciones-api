<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\Role;
use App\Http\Requests\PasswordResetRequest;
use Yajra\Datatables\Facades\Datatables;
class UserController extends Controller
{
    public function auth(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $user = User::where('email', '=', $credentials['email'])->first();
        $customClaims = ['datos' => $user];
        try {
          if (! $token = JWTAuth::attempt($credentials, $customClaims)) {
            return response()->json(['error' => 'invalid_credentials'], 401);
          }
        } catch (JWTException $e) {
          return response()->json(['error' => 'could_not_create_token'], 500);
        }
         return response()->json(compact('token','user'));
    }

    public function destroy($id){
        $user=User::findOrFail($id);
        $user->delete();
        return response()->json(compact('user'));
    }

    public function index(){
        $user=User::all();
        return response()->json(compact('user'));
    }

    public function show($id){
        $user=User::with(['role'])->findOrfail($id);
        return response()->json(compact('user'));
    }
    
     public function store(Request $request){
        $user=new User();
        $user->name=$request->name;
        $user->apepat=$request->apepat;
        $user->apemat=$request->apemat;
        $user->email=$request->email;
        $user->rut=$request->rut;
        $user->firm="";
        $user->password=bcrypt($request->password);
        $user->role_id=$request->role_id;
        $user->save();
        return response()->json(compact('user'));
    }

       public function profile($id,Request $request){
           $user=User::findOrFail($id);
           $user->name=$request->name;
           $user->apepat=$request->apepat;
           $user->apemat=$request->apemat;
           $user->email=$request->email;
           $user->save();
           return response()->json($user,200);
        }
    
        public function edit($id){
           $user=User::findOrFail($id);
           $roles=Role::all()->pluck('type', 'id');
           return response()->json(compact('user'));
        }
       
        public function change_passwd($id,PasswordResetRequest $request){
            $user=User::findOrFail($id);
            $current_password=bcrypt($request->current_password);
            $user->password=bcrypt($request->password);
            $user->save();
            return response()->json(compact('user'));
        }
       
        public function update($id,Request $request){
            $user=User::findOrFail($id);
            $user->name=$request->name;
            $user->apepat=$request->apepat;
            $user->apemat=$request->apemat;
            $user->rut=$request->rut;
            $user->firm="";
            $user->email=$request->email;
            $user->role_id=$request->role_id;
            $user->save();
            return response()->json(compact('user'));
        }
}
