<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stablishment;
use Yajra\Datatables\Facades\Datatables;
class StablishmentController extends Controller
{
    public function destroy($id){
        $stablishment=Stablishment::findOrFail($id);
        $stablishment->delete();
        return response()->json(compact('stablishment'));
    }

    public function index(){
        $stablishment=Stablishment::all();
        return Datatables::of($stablishment)
        ->addColumn('action', function($stablishment) {
          return '<a href="#" class="edit" data-id='.$stablishment->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$stablishment->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }

    public function show($id){
        $stablishment=Stablishment::findOrfail($id);
        return response()->json(compact('stablishment'));
    }


    
     public function store(Request $request){
        $stablishment=new Stablishment();
        $stablishment->name=$request->name;
        $stablishment->save();
        return response()->json(compact('stablishment'));
    }

       
    
        public function edit($id){
           $stablishment=Stablishment::findOrFail($id);
           return response()->json(compact('stablishment'));
        }
       
   
       
        public function update($id,Request $request){
            $stablishment=Stablishment::findOrFail($id);
            $stablishment->name=$request->name;
            $stablishment->save();
            return response()->json(compact('stablishment'));
        }
}
