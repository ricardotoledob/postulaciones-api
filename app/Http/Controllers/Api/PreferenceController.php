<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Preference;
use Yajra\Datatables\Facades\Datatables;

class PreferenceController extends Controller
{
    public function destroy($id){
        $preference=Preference::findOrFail($id);
        $preference->delete();
        return response()->json(compact('preference'));
    }

    public function index(){
        $preference=Preference::all();
        return Datatables::of($preference)
        ->addColumn('action', function($preference) {
          return '<a href="#" class="edit" data-id='.$preference->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$preference->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }



    public function show($id){
        $preference=Preference::findOrfail($id);
        return response()->json(compact('preference'));
    }
    
     public function store(Request $request){
        $preference=new Preference();
        $preference->publish_document_id=$request->publish_document_id;
        $preference->stablishment_id=$request->stablishment_id;
        $preference->save();
        return response()->json(compact('preference'));
    }


       
    
        public function edit($id){
           $preference=Preference::findOrFail($id);
           return response()->json(compact('preference'));
        }
       
   
       
        public function update($id,Request $request){
            $preference=Preference::findOrFail($id);
            $preference->publish_document_id=$request->publish_document_id;
            $preference->stablishment_id=$request->stablishment_id;
            $preference->save();
            return response()->json(compact('preference'));
        }
}
