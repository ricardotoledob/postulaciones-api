<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Document;
use Yajra\Datatables\Facades\Datatables;
class DocumentController extends Controller
{
    public function destroy($id){
        $documents=Document::findOrFail($id);
        $documents->delete();
        return response()->json(compact('documents'));
    }

    public function index(){
        $documents=Document::all();
        return Datatables::of($documents)
        ->addColumn('action', function($documents) {
          return '<a href="#" class="edit" data-id='.$documents->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$documents->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }

    public function show($id){
        $documents=Document::findOrfail($id);
        return response()->json(compact('documents'));
    }
    
    public function download_zip($publish_document_id){
        $documents=Document::where('publish_document_id','=',$publish_document_id)->get();
        foreach($documents as $document) {
            $files = glob($document->name);
            \Zipper::make(public_path($publish_document_id . ".zip"))->add($files)->close(); 
        } 
        return response()->download(public_path($publish_document_id . ".zip"));
    }

    public function remove_zip($publish_document_id){
       unlink(public_path($publish_document_id.".zip")); 
       return response()->json("Eliminado",200);
    }


     public function store(Request $request){
        $documents=new Document();
        if($request->file('file')!=null){
            $fileName = time()."-".$request->file('file')->getClientOriginalName();
            $path =$request->file('file')->move(public_path("/"),$fileName);
            $documentURL=url('/'.$fileName);
            $documents->file=$documentURL;
            $documents->name=$fileName;
        }else{
            $documents->file=$documentURL=url('/'."nofile.pdf");
            $documents->name="nofile.pdf";
        }
        $documents->title=$request->title;
        $documents->description=$request->description;
        $documents->publish_document_id=$request->publish_document_id;
        $documents->save();
        return response()->json(compact('documents'));
    }

       
    
        public function edit($id){
           $documents=Document::findOrFail($id);
           return response()->json(compact('documents'));
        }
       
   
       
        public function update($id,Request $request){
            $documents=Document::findOrFail($id);
            if($request->file('file')!=null){
                $fileName = time()."-".$request->file('file')->getClientOriginalName();
                $path =$request->file('file')->move(public_path("/"),$fileName);
                $documentURL=url('/'.$fileName);
                $documents->file=$documentURL;
                $documents->name=$fileName;
            }else{
                $documents->file=$documentURL=url('/'."nofile.pdf");
                $documents->name="nofile.pdf";
            }
            $documents->title=$request->title;
            $documents->description=$request->description;
            $documents->publish_document_id=$request->publish_document_id;
            $documents->save();
            return response()->json(compact('documents'));
        } 
}
