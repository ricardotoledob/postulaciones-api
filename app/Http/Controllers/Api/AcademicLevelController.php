<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AcademicLevel;
use Yajra\Datatables\Facades\Datatables;
class AcademicLevelController extends Controller
{
    public function destroy($id){
        $academics=AcademicLevel::findOrFail($id);
        $academics->delete();
        return response()->json(compact('academics'));
    }

    public function index(){
        $academics=AcademicLevel::all();
        return Datatables::of($academics)
        ->addColumn('action', function($academics) {
          return '<a href="#" class="edit" data-id='.$academics->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$academics->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }

    public function show($id){
        $academics=AcademicLevel::findOrfail($id);
        return response()->json(compact('academics'));
    }
    
     public function store(Request $request){
        $academics=new AcademicLevel();
        $academics->name=$request->name;
        $academics->save();
        return response()->json(compact('academics'));
    }

       
    
        public function edit($id){
           $academics=AcademicLevel::findOrFail($id);
           return response()->json(compact('academics'));
        }
       
   
       
        public function update($id,Request $request){
            $academics=AcademicLevel::findOrFail($id);
            $academics->name=$request->name;
            $academics->save();
            return response()->json(compact('academics'));
        } 
}
