<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profesion;
use Yajra\Datatables\Facades\Datatables;
class ProfesionController extends Controller
{
    public function destroy($id){
        $profesions=Profesion::findOrFail($id);
        $profesions->delete();
        return response()->json(compact('profesions'));
    }

    public function index(){
        $profesions=Profesion::all();
        return Datatables::of($profesions)
        ->addColumn('action', function($profesions) {
          return '<a href="#" class="edit" data-id='.$profesions->id.' data-toggle="modal" data-target="#myModalEdit"><i class="fa fa-edit"></i> </a>'
          . '<a href="#" class="delete" data-id='.$profesions->id.'><i class="fa fa-trash"></i>   </a>';
        })->make(true);
    }

    public function show($id){
        $profesions=Profesion::findOrfail($id);
        return response()->json(compact('profesions'));
    }
    
     public function store(Request $request){
        $profesions=new Profesion();
        $profesions->name=$request->name;
        $profesions->save();
        return response()->json(compact('profesions'));
    }

       
    
        public function edit($id){
           $profesions=Profesion::findOrFail($id);
           $roles=Role::all()->pluck('type', 'id');
           return response()->json(compact('profesions'));
        }
       
   
       
        public function update($id,Request $request){
            $profesions=Profesion::findOrFail($id);
            $profesions->name=$request->name;
            $profesions->save();
            return response()->json(compact('profesions'));
        } 
}
