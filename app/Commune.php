<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Commune extends Model
{
    use SoftDeletes;
    protected $table='communes';
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];

     public function province(){
         return $this->belongsTo('App\Province');
     }
}
