<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Profesion extends Model
{
    use SoftDeletes;
    protected $table='profesions';
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];
}
