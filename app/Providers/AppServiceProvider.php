<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->validator->extendImplicit('no_password', function ($attribute, $value, $parameters) {
            if (\Hash::check($value, auth()->user()->password)) {
                return true;
            }
            return false;
        }, 'The last password are wrong!');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
