<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'users';
    protected $fillable = [
        'name','rut','apepat','apemat','email', 'password','role_id'
    ];
   
    protected $hidden = [
        'remember_token',
    ];
   
    protected $dates = [
       'deleted_at',
       'created_at',
       'updated_at'
    ];
    public function role() {
       return $this->belongsTo('App\Role');
    }
}
