<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Province extends Model
{
    use SoftDeletes;
    protected $table='provinces';
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];

     public function region(){
         return $this->belongsTo('App\Region');
     }
}
