<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use SoftDeletes;
    protected $table='regions';
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];
     
}
