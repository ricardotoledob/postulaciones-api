<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PublishDocument extends Model
{
    use SoftDeletes;
    protected $table='publish_documents';
    public $fillable = ['id'];
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
     ];

     public function profesion(){
         return $this->belongsTo('App\Profesion');
     }

     public function commune(){
         return $this->belongsTo('App\Commune');
     }
     public function academic_level(){
         return $this->belongsTo('App\AcademicLevel');
     }

     public function documents(){
         return $this->hasMany('App\Document');
     }

     //NUEVO

     public function preferences(){
         return $this->hasMany('App\Preference');
     }

     
}
